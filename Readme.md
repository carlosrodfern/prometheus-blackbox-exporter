# How to build locally

```sh
spectool -g prometheus-blackbox-exporter.spec
rpkg srpm --outdir .
mock -r centos-stream-9-x86_64 --init
mock --enable-network -r centos-stream-9-x86_64 *.src.rpm
```

# How to test

```sh
cp /var/lib/mock/centos-stream-9-x86_64/result/prometheus-blackbox-exporter-0.24.0-3.el9.x86_64.rpm ./tests/data/prometheus-blackbox-exporter.rpm
tmt -vvv run
```