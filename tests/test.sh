#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "dnf install -y ./data/prometheus-blackbox-exporter.rpm"
        rlServiceStart "prometheus-blackbox-exporter.service"
    rlPhaseEnd

    rlPhaseStartTest
        rlWaitForCmd "curl -s http://localhost:9115/-/healthy" -t 60
        rlRun -s "curl -s -w '%{http_code}' http://localhost:9115/-/healthy"
        rlAssertGrep "200" $rlRun_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlServiceStop "prometheus-blackbox-exporter.service"
        rlRun "dnf remove -y prometheus-blackbox-exporter"
    rlPhaseEnd
rlJournalEnd
