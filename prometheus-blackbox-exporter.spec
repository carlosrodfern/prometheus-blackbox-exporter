# https://github.com/prometheus/blackbox_exporter
%global goipath         github.com/prometheus/blackbox_exporter
Version:                0.24.0
%global tag             v0.24.0

%gometa -f

%global goname prometheus-blackbox-exporter

%global common_description %{expand:
The blackbox exporter allows blackbox probing of endpoints over HTTP, HTTPS, DNS, TCP, ICMP and gRPC.}

%global golicenses      LICENSE NOTICE\\\
%global godocs          CHANGELOG.md CODE_OF_CONDUCT.md CONFIGURATION.md\\\
                        MAINTAINERS.md README.md SECURITY.md

Name:           %{goname}
Release:        3%{?dist}
Summary:        The blackbox exporter allows blackbox probing of endpoints over HTTP, HTTPS, DNS, TCP, ICMP and gRPC

License:        Apache-2.0
URL:            %{gourl}
Source0:        %{gosource}
Source1:        %{goname}.sysusers
Source2:        %{goname}.service
Source3:        %{goname}.conf

BuildRequires:  systemd-rpm-macros
Requires(pre): shadow-utils

%description %{common_description}

%gopkg

%prep
%goprep -k
%autopatch -p1

%build
export GO111MODULE=on
%gobuild -o %{gobuilddir}/bin/%{goname} %{goipath}

%install
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/
install -m 0644 -vpD %{S:1} %{buildroot}%{_sysusersdir}/%{goname}.conf
install -m 0644 -vpD %{S:2} %{buildroot}%{_unitdir}/%{goname}.service
install -m 0644 -vpD %{S:3} %{buildroot}%{_sysconfdir}/default/%{goname}
install -m 0644 -vpD blackbox.yml %{buildroot}%{_sysconfdir}/%{goname}/blackbox.yml
mkdir -vp %{buildroot}/%{_mandir}/man1/
%{buildroot}%{_bindir}/%{goname} --help-man > %{buildroot}/%{_mandir}/man1/%{goname}.1
sed -i '/^  /d; /^.SH "NAME"/,+1c.SH "NAME"\nprometheus-blackbox-exporter \\- The Prometheus Blackbox Exporter' \
    %{buildroot}/%{_mandir}/man1/%{goname}.1

%pre
%sysusers_create_compat %{SOURCE1}

%post
%systemd_post %{goname}.service

%preun
%systemd_preun %{goname}.service

%postun
%systemd_postun_with_restart %{goname}.service

%files
%license LICENSE NOTICE
%doc CHANGELOG.md CODE_OF_CONDUCT.md CONFIGURATION.md
%doc MAINTAINERS.md README.md SECURITY.md
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/default/%{goname}
%dir %{_sysconfdir}/%{goname}/
%config(noreplace) %{_sysconfdir}/%{goname}/blackbox.yml
%{_unitdir}/%{goname}.service
%{_sysusersdir}/%{goname}.conf
%{_mandir}/man1/%{goname}.1*

%changelog
%autochangelog
